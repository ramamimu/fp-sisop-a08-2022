#include <bits/stdc++.h>
#include <vector>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

using namespace std;

#define PORT 3001
// source daemon code https://ofstack.com/C++/28351/c++-write-linux-daemon-implementation-code.html
// data
typedef struct UserData{
    string uname;
    string pass;
    string useDatabase;
    bool isValidUse=false;
    bool isRoot=false;
    int status=0;
    string error_status;
    string warning;
}users;

vector <string> command;

users user;

// socket
int socketID;

typedef struct message
{
    int charRange;
    char msg[1024];
}msg;

void send_msg(char input[]){
    send(socketID, input, 1024, 0);
}

msg rcv_msg(){
    msg get_msg;
    // char buffer[1024]={0};
    get_msg.charRange = read( socketID , get_msg.msg, 1024);
    // printf("%d %s %ld\n", ambil, setering, strlen(setering) );
    printf("has ben nerimo: %s\n", get_msg.msg);
    return get_msg;
}

// tools
void init(){
    user.uname ="";
    user.pass ="";
    user.useDatabase ="";
    user.error_status="";
    user.warning ="";
    user.isRoot =false;
    user.status =0;
}

char *string_to_char(string word){
    char *temp =new char[word.length() + 1]; 
    strcpy(temp, word.c_str()); 
    return temp;
}

void status_validate(int status, string error_status, string warning){
    user.status = status;
    user.error_status = error_status;
    user.warning = warning;
}

void cleanWord(bool status_coma){
    for(int i=0; i<command.size();i++){
        if(command[i].back() == ';'){
            command[command.size()-1].pop_back();
        }
        if(command[i].back() == ')'){
            command[command.size()-1].pop_back();            
        }
        if(command[i][0] == ')' || command[i][0] == '(' ){
            command[i].erase(0, 1);
        }
        if(status_coma){
            if(command[i][0] == ','){
                command[i].erase(0, 1);
            }
            if(command[i].back() == ',' ){
                command[i].erase(command[i].find(','), 1);
            }
        }
    }
}

void spellWords(string str)
{
    string word;
    stringstream iss(str); 
    while (iss >> word){
        cout << word << endl;
        command.push_back(word);
    }
    cleanWord(false);
    for(auto i : command){
        cout<< i<<endl;
    }
}

// case logic
bool user_exist(){
    FILE *fp = fopen("/home/ram/sisop/FP/jawaban/databaseku/databases/user/users.txt", "r");
    char buffer[1024];
    bool res=false;
    while (fgets(buffer, 1024, fp) != NULL )
    {
        char formatUser[50];
        strtok(buffer, "\n");
        sprintf(formatUser,"%s,%s", string_to_char(user.uname), string_to_char(user.pass));
        if(strcmp(buffer, formatUser)==0) {
            // printf("oke sesuai\n");
            res=true;
            break;
        }
    }
    fclose(fp);
    return res;
}

bool permitted(){
    FILE *fp = fopen("/home/ram/sisop/FP/jawaban/databaseku/databases/user/permission.txt", "r");
    char buffer[1024];
    int res=false;
    // cout<<user.uname<<","<<user.useDatabase<<endl;
    while (fgets(buffer, 1024, fp) != NULL )
    {
        char formatUser[100];
        strtok(buffer, "\n");
        sprintf(formatUser,"%s,%s", string_to_char(user.uname),string_to_char( user.useDatabase));
        if(strcmp(buffer, formatUser)==0) {
            res=true;
            break;
        }
    }
    fclose(fp);
    return res; 
}

void add_user(char username[], char password[]){
    FILE *fp = fopen("/home/ram/sisop/FP/jawaban/databaseku/databases/user/users.txt", "a");
    char buffer[1024];
    sprintf(buffer, "\n%s,%s", username, password);
    fprintf(fp,"%s",buffer);
    fclose(fp);
}

void add_grant_permission(char username[], char database[]){
    FILE *fp = fopen("/home/ram/sisop/FP/jawaban/databaseku/databases/user/permission.txt", "a");
    char buffer[1024];
    sprintf(buffer, "\n%s,%s", username, database);
    fprintf(fp,"%s",buffer);
    fclose(fp);
}

bool command_on_ddl(){
    if(command.size() == 3 && command[0]=="CREATE" && command[1]=="DATABASE"){
        add_grant_permission( string_to_char(user.uname), string_to_char(command[2]));
        return true;
    }
    
    if(user.useDatabase!=""){
        if(command.size() >= 3 && command[0]=="CREATE" && command[1]=="TABLE"){
            cleanWord(true);
            return true;
        }
        if(command.size() >= 2 && command[0]=="DROP"){
            return true;
        }
        if(command.size() >= 4 && command[0]=="INSERT" && command[1]=="INTO"){
            cleanWord(true);
            return true;
        }
        if(command.size() == 4 && command[0]=="UPDATE" && command[2]=="SET"){
            return true;
        }
        if(command.size() == 3 && command[0]=="DELETE" && command[1]=="FROM"){
            return true;
        }
        if(command.size() == 5 && command[0]=="DELETE" && command[1]=="FROM" && command[3]=="WHERE"){
            return true;
        }
    }

    return false;
}

void process(){
    if(command[0]=="USER"){
        // USER root
        // USER uname pass
        init();
        if(command[1]=="root"){
            user.isRoot = true;
            user.uname = command[1];
            user.pass = command[1];
            status_validate(1, "1", "");
        } else{
            if(command.size() == 3){
                user.uname = command[1];
                user.pass = command[2];
            }else{
                status_validate(0, "login", "username or/and password wrong");
                return;
            }
            if(!user_exist()){
                status_validate(0, "login", "username or/and password wrong");
                return;
            }
        }
        status_validate(1,"1","");
    } else if(command[0]=="USE"){
        if(command.size() == 2){
            user.useDatabase = command[1];
            if(!permitted()){
                user.isValidUse=false;
                status_validate(0, "USE DB", "You dont have any permission");
                return;
            }
        }else if (user.status==0){
            user.isValidUse=false;
            return;
        }else{
            user.isValidUse=false;
            status_validate(0, "USE DB", "pattern/format do not recognize");
            return;
        }
        user.isValidUse=true;
        status_validate(1,"1","");
    } else if(command.size()==6 && command[0]=="CREATE" && command[1]=="USER" && command[3]=="IDENTIFIED" && command[4]=="BY"){
        if(user.isRoot){
            add_user(string_to_char(command[2]), string_to_char(command[5]));
            status_validate(1, "1","");
        }else{
            status_validate(0, "CREATE USER", "You do not have permission");
        }
    } else if(command.size()==5 && command[0]=="GRANT" && command[1]=="PERMISSION" && command[3] == "INTO"){
        if(user.isRoot){
            add_grant_permission(string_to_char(command[4]), string_to_char(command[2]));
            status_validate(1, "1","");
        }else{
            status_validate(0, "GRANT PERMISSION", "You do not have permission");
        }
    } else if(command_on_ddl()){
        string temp_runner="gcc database.c -o database && ./database";

        string temp_text=user.useDatabase;
        transform(temp_text.begin(), temp_text.end(), temp_text.begin(), ::tolower);

        temp_runner = temp_runner+" "+temp_text;

        for(int i=0; i<command.size(); i++){
            temp_text=command[i];
            transform(temp_text.begin(), temp_text.end(), temp_text.begin(), ::tolower);
            temp_runner=temp_runner+" "+temp_text;
        }
        system(string_to_char(temp_runner));
        // cout<<temp_runner<<endl;
        status_validate(0, "1","");
    }

    else if(command[0]=="SEE"){
        if(command.size()==2){
            if(command[1]=="USEDB"){
                if(user.isValidUse){
                    status_validate(1, "WATCH", user.useDatabase);
                }else{
                    status_validate(1, "WATCH", "Database is not be validated");
                }
                return;
            }else if(command[1]=="USER"){
                status_validate(1, "WATCH", user.uname);
            }
        }else{
            status_validate(0, "WATCH", "pattern/format do not recognize");
            return;
        }
    }
    else if(command[0]=="exit"){
        init();
        status_validate(1, "1", "");
    }
    else{
        if(user.useDatabase == ""){
            status_validate(0, "USE DB", "Please select your use DB!");
        }else{
            status_validate(0, "COMMAND", "pattern/format do not recognize");
        }
        return;
    }
}


static bool flag = true;
void handler(int sig)
{
	printf("I got a signal %d\nI'm quitting.\n", sig);
	flag = false;
}

int main(int argc, char const *argv[]) {
	time_t t;
	int fd;
	if(-1 == daemon(0, 0))
	{
		printf("daemon error\n");
		exit(1);
	}
	struct sigaction act;
	act.sa_handler = handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	if(sigaction(SIGQUIT, &act, NULL))
	{
		printf("sigaction error.\n");
		exit(0);
	}

    // socket
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
        
    while(1){
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        } else {
            socketID = new_socket;
        }

        msg uji;
        uji = rcv_msg();
        command.clear();
        spellWords(uji.msg);
        process();
        char *temp=string_to_char(user.error_status + ": " + user.warning);
        send_msg(temp);            

        // status = 0 not okay
        // status = 1 okay
        sleep(30);
    }

    return 0;
}
