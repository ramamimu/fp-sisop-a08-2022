#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <ctype.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

char path[256] = "/home/ram/sisop/FP/jawaban/databaseku/databases/";
char message[512];
char active_db[16];
char content[1024];

// Utility Function
char *toLower(char *str, size_t len){
    char *str_l = calloc(len+1, sizeof(char));

    for (size_t i = 0; i < len; ++i) {
        str_l[i] = tolower((unsigned char)str[i]);
    }
    return str_l;
}

int get_file_content_by_line(char *dirname, int read_line){
    // Source : https://github.com/portfoliocourses/c-example-code/blob/main/read_line.c
    int MAX_LINE = 1024;
    char buffer[MAX_LINE];

    FILE * file = fopen(dirname, "r");

    bool keep_reading = true;
    int current_line = 1;
    do {
        fgets(buffer, MAX_LINE, file);

        if (feof(file)){
            keep_reading = false;
            return 0;
        } else if (current_line == read_line){
            keep_reading = false;
            strcpy(content, buffer);
            return 1;
        }

        current_line++;

    } while (keep_reading);
    
    fclose(file);
}

// Database Function
int find_database(char *str){
    DIR *folder;
    struct dirent *ep;

    folder = opendir(path);

    if (folder != NULL){
        while ((ep = readdir (folder))){
            if(strcmp(ep->d_name, str) == 0){
                closedir(folder);
                return 1;
            }
        }
    }
    closedir(folder);
    return 0;
}

int find_table(char *str){
    DIR *folder;
    struct dirent *ep;

    char dirpath[256];
    strcpy(dirpath, path);
    strcat(dirpath, active_db);

    folder = opendir(dirpath);

    if (folder != NULL){
        while ((ep = readdir (folder))){
            if(strcmp(ep->d_name, str) == 0){
                closedir(folder);
                return 1;
            }
        }
    }
    closedir(folder);
    return 0;
}

int get_table_column(char *str){
    char dirname[256];
    strcpy(dirname, path);
    strcat(dirname, active_db);
    strcat(dirname, "/");
    strcat(dirname, str);

    FILE * file;

    file = fopen(dirname, "r");
    char file_content[32];

    int token = 1;

    if (file) {
        while (fscanf(file, "%s", file_content)!=EOF){
            if(token == 10){
                fclose(file);
                return atoi(file_content);
            }
            token++;
        }
    }
}

int find_column(char *str, char *dirname, char *column){
    get_file_content_by_line(dirname, get_table_column(str) + 5);

    char *delimiter = strtok(content, " |\t");
    while(delimiter != NULL){
        if(strcmp(delimiter, column) == 0){
            return 1;
        }

        delimiter = strtok(NULL, " |\t");
    }

    return 0;
}

/* DDL */
int CREATE_DATABASE(char *str){
    char dirname[256];
    strcpy(dirname, path);
    strcat(dirname, str);
    
    if(!find_database(str)){
        mkdir(dirname,0777);
        strcat(message, "\n-- Directory created\n");
        strcat(message, "-- Database succesfully created\n");
        return 1;
    } else {
        strcat(message, "\n-- Unable to create directory\n");
        strcat(message, "-- Database already exist");
        return 0;
    }

}

int DROP_DATABASE(char *str){
    char cmd[256];
    strcpy(cmd, "rm -r ");
    strcat(cmd, path);
    strcat(cmd, str);
    
    if(find_database(str)){
        system(cmd);
        strcat(message, "\n-- Drop database successfull\n");
        return 1;
    } else {
        strcat(message, "\n-- Database does not exist, drop database failed\n");
        return 0;
    }
}

int CREATE_TABLE(char *str, char *column){
    char dirname[256];
    strcpy(dirname, path);
    strcat(dirname, active_db);
    strcat(dirname, "/");
    strcat(dirname, str);

    if(!find_table(str)){
        FILE * file;

        file = fopen(dirname, "a");
        fprintf(file, "-- Table name : %s\n", str);
        
        char temp[256], temp1[256];
        strcpy(temp, column);
        strcpy(temp1, column);
        char *delimiter = strtok(temp, " ");
        int token = 0;
        while(delimiter != NULL){
            token++;
            delimiter = strtok(NULL, " ");
        }
        fprintf(file, "-- Total columns : %d\n", token/2);

        fprintf(file, "-- Data Type : \n");
        delimiter = strtok(temp1, " ");
        token = 0;
        while(delimiter != NULL){
            if(token%2==0){
                fprintf(file, "\t%s ", delimiter);
            } else {
                fprintf(file, "%s\n", delimiter);
            }
            token++;
            delimiter = strtok(NULL, " ");
        }
        fprintf(file, "\n");

        fprintf(file, "|");

        token = 0;
        delimiter = strtok(column, " ");
        while(delimiter != NULL){
            if(token%2 == 0){
                fprintf(file, "\t%s\t|", delimiter);
            }
            token++;
            delimiter = strtok(NULL, " ");
        }
        fprintf(file, "\n\n");
        fclose(file);

        strcat(message, "\n-- Create table successfull\n");
        return 1;
    } else {
        strcat(message, "\n-- Unable to create table in ");
        strcat(message, str);
        strcat(message, "\n-- Table already exist\n");
        return 0;
    }
}

int DROP_TABLE(char *str){
    char cmd[256];
    strcpy(cmd, "rm ");
    strcat(cmd, path);
    strcat(cmd, active_db);
    strcat(cmd, "/");
    strcat(cmd, str);

    if(find_table(str)){ // -- errror handling
        system(cmd);

        strcat(message, "\n Drop table succesfull\n");
        return 1;
    } else {
        strcat(message, "\n-- Table does not exist, drop table failed\n");
        return 0;
    }
}

int DROP_COLUMN(char *str, char *col_name){
    char dirname[256];
    strcpy(dirname, path);
    strcat(dirname, active_db);
    strcat(dirname, "/");
    strcat(dirname, str);

    char outdir[256];
    strcpy(outdir, path);
    strcat(outdir, active_db);
    strcat(outdir, "/");
    strcat(outdir, str);
    strcat(outdir, "_drop");

    if(find_table(str)){
        if(find_column(str, dirname, col_name)){
            // FILE * out;

            // out = fopen(outdir, "a");

            // int status = 1;
            // int token = 1;

            // char temp[256];
            // while(status == 1){
            //     status = get_file_content_by_line(dirname, token);
            //     int col_del = 1;
            //     char temp[256], temp1[256], temp2[256];
            //     strcpy(temp, content);
            //     strcpy(temp1, content);
            //     strcpy(temp2, content);
            //     char *delimiter;
            //     int steps, a, b;

            //     if(token == 1 || token == 8 || token == 3 || token == 10){
            //         fprintf(out, "%s", content);
            //     } else if(token == 2){
            //         steps = 1;
            //         delimiter = strtok(content, "- :");
            //         while(delimiter != NULL){
            //             if(steps == 3){
            //                 a = atoi(delimiter);
            //             }
            //             steps++;
            //             delimiter = strtok(NULL, "- :");
            //         }

            //         fprintf(out, "-- Total columns : %d\n", a-1);
            //     } else if(token > 3 && token < a + 4) {
            //         delimiter = strtok(content, " \t");
            //         if(!(strcmp(delimiter, col_name) == 0)){
            //             fprintf(out, "%s", temp);
            //         }
            //     } else if(token == a + 5){
            //         delimiter = strtok(content, "| \t\n");
            //         steps = 1;
            //         fprintf(out, "|");
            //         while(delimiter != NULL){
            //             if(!(strcmp(delimiter, col_name) == 0)){
            //                 fprintf(out, "\t%s\t|", delimiter);
            //             } else {
            //                 b = steps;
            //             }
            //             steps++;
            //             delimiter = strtok(NULL, "| \t\n");
            //         }
            //         fprintf(out, "\n");
            //     } else if(token > a + 5){
            //         delimiter = strtok(content, "| \t\n");
            //         steps = 1;
            //         while(delimiter != NULL){
            //         printf("%d %d\n", steps, b);
            //             if(steps!=b){
            //                 fprintf(out, "|\t%s\t", delimiter);
            //             } 
            //             steps++;
            //             delimiter = strtok(NULL, "| \t\n");
            //         }
            //         fprintf(out, "|\n");
            //     }
            //     token++;
            // }

            // char cmd[256];
            // strcpy(cmd, "rm ");
            // strcat(cmd, dirname);
            // system(cmd);

            // strcpy(cmd, "mv ");
            // strcat(cmd, outdir);
            // strcat(cmd, " ");
            // strcat(cmd, dirname);
            // system(cmd);

            strcat(message, "\n-- Under construction\n");
            
            return 1;
        } else {
            strcat(message, "\n-- Unable to drop column in table ");
            strcat(message, str);
            strcat(message, "\n-- Column does not exist\n");
            return 0;
        }
    } else {
        strcat(message, "\n-- Unable to drop column in table ");
        strcat(message, str);
        strcat(message, "\n-- Table does not exist\n");
        return 0;
    }
}

/* DML */
int INSERT_INTO(char *str, char *row, int column_row_numbers){
    char dirname[256];
    strcpy(dirname, path);
    strcat(dirname, active_db);
    strcat(dirname, "/");
    strcat(dirname, str);

    if(find_table(str)){
        printf("%d\n", get_table_column(str));
        if(get_table_column(str) == column_row_numbers){
            FILE * file;

            file = fopen(dirname, "a");

            char *delimiter = strtok(row, " ");
            int token = 0;

            fprintf(file, "| ");

            while(delimiter != NULL){
                fprintf(file, "%s\t| ", delimiter);

                token++;
                delimiter = strtok(NULL, " ");
            }
            fprintf(file, "\n");
            fclose(file);

            strcat(message, "\n-- Insert into table successfull\n");
            return 1;
        } else {
            strcat(message, "\n-- Unable to insert into table ");
            strcat(message, str);
            strcat(message, "\n-- Total value does not match with number table column\n");
            return 0;
        }
    } else {
        strcat(message, "\n-- Unable to insert into table ");
        strcat(message, str);
        strcat(message, "\n-- Table does not exist\n");
        return 0;
    }
}

int main(int argc, char** argv){
    for(int i=1; i< argc; i++){
        size_t len = strlen(argv[i]);
        argv[i] = toLower(argv[i], len);
        
        // printf("%s ", argv[i]);
    }
    // printf("\n");

    int status;

    if(strcmp(argv[1], "create") == 0 && strcmp(argv[2], "database") == 0){
        // CREATE DATABASE
        // Format : CREATE TABLE table_name
        strcat(message, "-- Database name : ");
        strcat(message, argv[3]);
        status = CREATE_DATABASE(argv[3]);
    } else if(strcmp(argv[1], "drop") == 0 && strcmp(argv[2], "database") == 0){
        // DROP DATABASE
        // Format : DROP TABLE table_name
        strcat(message, "-- Database name : ");
        strcat(message, argv[3]);
        status = DROP_DATABASE(argv[3]);
    } else if(!(strcmp(argv[1], "create") == 0 || strcmp(argv[1], "drop") == 0)){
        strcpy(active_db, argv[1]); // Database used
        if(strcmp(argv[2], "create") == 0 && strcmp(argv[3], "table") == 0){
            // CREATE TABLE
            // Format : database_used CREATE TABLE table_name column1 type column2 type ... (no symbol - separated by space)
            if((argc-5)%2 == 0){     // -- error handling
                strcat(message, "-- Table name : ");
                strcat(message, argv[4]);

                char column[256];
                for(int i=5; i<argc; i++){
                    strcat(column, argv[i]);
                    strcat(column, " ");
                }
                status = CREATE_TABLE(argv[4], column);

            } else {
                strcat(message, "-- Too few argument in table(..), please enter the right database command\n");
            }

        } else if(strcmp(argv[2], "drop") == 0 && strcmp(argv[3], "table") == 0){
            // DROP TABLE
            // Format : database_used DROP TABLE table_name
            strcat(message, "-- Table name : ");
            strcat(message, argv[4]);
            status = DROP_TABLE(argv[4]);
        } else if(strcmp(argv[2], "insert") == 0 && strcmp(argv[3], "into") == 0){
            // INSERT INTO
            // Format : database_used INSERT INTO table_name column1 column2 ... (no symbol - separated by space)
            strcat(message, "-- Table name : ");
            strcat(message, argv[4]);

            char row[256];
            for(int i=5; i<argc; i++){
                strcat(row, argv[i]);
                strcat(row, " ");
            }
            status = INSERT_INTO(argv[4], row, argc-5);
        } else if(strcmp(argv[2], "drop") == 0 && strcmp(argv[3], "column") == 0){
            if(strcmp(argv[5], "from") == 0){
                // DROP COLUMN
                // Format : database_used DROP COLUMN column_name FROM table_name
                strcat(message, "-- Table name : ");
                strcat(message, argv[6]);
                status = DROP_COLUMN(argv[6], argv[4]);

            } else {
                strcat(message, "-- Command wrong, please enter the right database command\n");
                status = 0;
            }
        } else {
            strcat(message, "-- Command wrong, please enter the right database command\n");
            status = 0;
        }
    } else {
        strcat(message, "-- Command wrong, please enter the right database command\n");
        status = 0;
    }

    printf("%s\n", message);

    return 0;
}

