#include <bits/stdc++.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

using namespace std;

#define PORT 3001

typedef struct UserData{
    char *uname;
    char *pass;
}users;

users user;

// define socket
int socketID;

typedef struct message
{
    int charRange;
    char msg[1024];
}msg;

void send_msg(char input[]){
    send(socketID, input, 1024, 0);
}

msg rcv_msg(){
    msg get_msg;
    char buffer[1024]={0};
    get_msg.charRange = read( socketID , get_msg.msg, 1024);
    // printf("%d %s %ld\n", ambil, setering, strlen(setering) );
    return get_msg;
}
// batas socket

int main(int argc, char const *argv[]) {
    msg message;
    bool ever=false;
    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;
    
    while (1)
    {
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            printf("\n Socket creation error \n");
            return -1;
        } else{
            socketID = sock;
        }

        memset(&serv_addr, '0', sizeof(serv_addr));

        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(PORT);
        if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
            printf("\nInvalid address/ Address not supported \n");
            return -1;
        }
        if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
            printf("\nConnection Failed \n");
            return -1;
        }
        
        // uname and pass
        // if not root
        if(!ever){
            cout<<"LOOP\n";
            if (getuid()){
                string login;
                login="USER ";
                
                if(argc != 5){
                    printf("format salah\n");
                    return 0;
                } else{
                    int algoritma;
                    for (int i = 0; i < argc; i++)
                    {   
                        if(i==1){
                            if(strcmp(argv[i],"-u")!=0){
                                printf("format salah\n");
                                return 0;
                            }else{
                                login += argv[i+1];
                            }
                        }
                        else if(i==3){
                            if(strcmp(argv[i],"-p")!=0){
                                printf("format salah\n");
                                return 0;
                            } else{
                                login =login+" "+ argv[i+1];
                            }
                        }
                    }
                }
                char *temp =new char[login.length() + 1]; 
                strcpy(temp, login.c_str()); 
                send_msg(temp);
            }
            // if root
            else{
                char temp[10] = "USER root";
                send_msg(temp);
            } 
            message = rcv_msg();
            if(strcmp(message.msg,"1: ")!=0 ){
                cout << message.msg 
                     << endl;
            }else{
                cout<<"message: success\n";
            }
            ever=true;
        } else{           
            bool is_exit=false; 
            char input[200];
            fgets(input, sizeof(input), stdin);
            printf("==> %s\n", input);
            send_msg(input);
            
            message = rcv_msg();
            if(strcmp(message.msg,"1: ")!=0 ){
                cout << message.msg 
                     << endl;
            }else{
                cout<<"message: success\n";
            }

            char *temp=input;     
            if(strcmp(temp,"exit\n")==0){
                return 0;
            }
        }

    }
}
